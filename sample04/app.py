#!/usr/bin/env python
from flask import Flask, render_template, Response
from camera_opencv import Camera
from kafka import KafkaConsumer
import config

app = Flask(__name__)

consumer = KafkaConsumer(
    config.TOPIC,
    bootstrap_servers=config.KAFKA_SERVER
)

@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    for msg in consumer:
        # Read frame from Kafka
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + msg.value + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
