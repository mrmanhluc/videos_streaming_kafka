#!/usr/bin/env python
import cv2 as cv
face_cascade = cv.CascadeClassifier('vision_api/face_detector_sys/haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier('vision_api/face_detector_sys/haarcascade_eye.xml')


def face_detect(img=None, test=False):
    # if img is None:
    if img is None:
        img = cv.imread('vision_api/face_test.jpg')

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex, ey, ew, eh) in eyes:
            cv.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

    # if test:
    #     cv.imshow('img', img)
    #     cv.waitKey(0)
    #     cv.destroyAllWindows()

    return img
#
#
# if __name__ == '__main__':
#     face_detect(None, True)
