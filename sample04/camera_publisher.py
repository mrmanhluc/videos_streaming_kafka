import cv2
from base_camera import BaseCamera
from vision_api.face_detector import face_detect
from kafka import KafkaProducer
import config


class Camera(BaseCamera):
    video_source = 0

    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()

            # encode as a jpeg image and return it
            img = face_detect(img, False)
            yield cv2.imencode('.jpg', img)[1].tobytes()


class CameraProducer:
    def __init__(self, topic, server_add):
        self.topic = topic
        self.producer = KafkaProducer(bootstrap_servers=server_add)

    def publish(self):
        while True:
            camera = Camera()
            frame = camera.get_frame()
            self.producer.send(self.topic, frame)


if __name__ == '__main__':
    cam = CameraProducer(config.TOPIC, config.KAFKA_SERVER)
    cam.publish()
