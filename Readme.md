# Live Videos Processing

## I. Overview

### 1. What's Streaming
* **Very large responses.** Having to assemble a response in memory only to return it to the client can be inefficient for very large responses. An alternative would be to write the response to disk and then return the file with flask.send_file(), but that adds I/O to the mix. Providing the response in small portions is a much better solution, assuming the data can be generated in chunks.

* **Real time data.** For some applications a request may need to return data that comes from a real time source. A pretty good example of this is a real time video or audio feed. A lot of security cameras use this technique to stream video to web browsers.
### 2. Idea's YIELD
Flask sử dụng ý tưởng từ Yield, modified yield và tạo function để xử lý streaming.

Yield ra đời xuất phát từ thực tế khi Return 1 lúc nhiều giá trị, lượng Ram lớn sẽ bị chiếm dụng và có thể dẫn tới Overload.
Vì vậy yield cho phép return thành nhiều step trong 1 function.

```python
def videos_processing():
    yield "frame1"
    yield "frame2"
    yield "frame3"
    
videos = videos_processing()

# Cach 1
videos.__next__()
videos.__next__()
videos.__next__()

# Cach 2
for video in videos:
    print(video)
```

Output : 
````python
frame1
frame2
frame3
````

Ví dụ khác trong thực tế có sử dụng Yield để hiển thị giá cổ phiếu.

Function dưới generate toàn bộ giá cổ phiếu.
Với mỗi lần reload, web sẽ load 1 phần của Stock mà không cần phải load toàn bộ lên Memory.

```python
from flask import Response, render_template
from app.models import Stock

def generate_stock_table():
    yield render_template('stock_header.html')
    for stock in Stock.query.all():
        yield render_template('stock_row.html', stock=stock)
    yield render_template('stock_footer.html')

@app.route('/stock-table')
def stock_table():
    return Response(generate_stock_table())
```


### 3. Multipart Responses
Ý tưởng chính thực hiện real-time trên web là : 
```python
in-place updates is to use a multipart response
```
Ví dụ multipart response trên web.
```python
HTTP/1.1 200 OK
Content-Type: multipart/x-mixed-replace; boundary=frame

--frame
Content-Type: image/jpeg

<jpeg data here>
--frame
Content-Type: image/jpeg

<jpeg data here>
...
```
Nội dung content-type được định nghĩa dạng  multipart/x-mixed-replace;

Trong đó, mỗi frame sẽ chứa các nội dung nhỏ hơn. Từng phần của nội dung nhỏ này sẽ lần lượt được hiển thị lên web.

### 4. Build Live Streaming Videos

![alt text](./img/transport_compress.gif)

#### 4.1 Transport
Để transport videos, videos cần được nén trước khi truyển tải.

Có nhiều phương pháp nén videos khác nhau, trong đó gồm : 
- Motion JPEG : Được sử dụng nhiều ở Security Cam, được phát triển năm 1990. Ưu : Độ trễ thấp. Nhược : Chất lượng hình ảnh kém, không truyền được âm thanh, yêu cầu băng thông cao

- H.264 : Chuẩn nén hình ảnh mới phát triển từ năm 2003. Ưu : Truyền hình ảnh chất lượng cao, độ trễ thấp, đồng bộ được âm thanh. Nhược : Tốn tài nguyên giải nén hình ảnh.

Còn nhiều chuẩn khác nhau, tham khảo thêm trong link bên dưới.


#### 4.2 Protocol (Giao thức) truyền videos
Một số phương thức phổ biến trong truyền videos : RTSP, RTMP, HTTP, HLS, IGMP.
- RTSP (Real-time Streaming Protocol) : Phương thức truyền videos real time. Lưu ý một số ip camera không cho phép truyền theo phương thức này
- 


### 5. Practice

#### Sample 01 : Tạo web Flash hiển thị real time hình ảnh 1,2,3 jpg. Chuẩn [Motion JPEG]

```python
#!/usr/bin/env python
from flask import Flask, render_template, Response
from camera import Camera

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


def gen(camera):
    while True:
        frame = camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
```
index.html
```python
<html>
  <head>
    <title>Video Streaming Demonstration</title>
  </head>
  <body>
    <h1>Video Streaming Demonstration</h1>
    <img src="{{ url_for('video_feed') }}">
  </body>
</html>
```

#### Flows running : 
- **Step 1 :** SRC tag tại index.html trên web được load bằng cách gọi tới function route "video_feed" và sẽ nhận được streaming response
- **Step 2 :** Function video_feed return về URL nhận được từ function Camera()
- **Step 3 :** Function Camera() sẽ return dạng Yield chứa URL của image và lặp vô hạn. Mỗi vòng lặp sẽ trả về 1 image

#### Kết quả : 
![alt text](./img/img1.png)


---
#### Sample 2 : Thay Function camera return image 1,2,3.jpg bằng function đọc từ OpenCV.

camera_opencv.py
```python
import cv2
from base_camera import BaseCamera


class Camera(BaseCamera):
    video_source = 0

    @staticmethod
    def set_video_source(source):
        Camera.video_source = source

    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()

            # encode as a jpeg image and return it
            yield cv2.imencode('.jpg', img)[1].tobytes()
```

Kết quả : 

![alt text](./img/img2.png)


#### 3. Sample 3. Build real-time face detection

Step 1 : Add function face_detector.py
```python
#!/usr/bin/env python
import cv2 as cv
face_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier('haarcascade_eye.xml')

def face_detect(img=None, test=False):
    # if img is None:
    if img is None:
        img = cv.imread('vision_api/face_test.jpg')

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]
        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex, ey, ew, eh) in eyes:
            cv.rectangle(roi_color, (ex, ey), (ex+ew, ey+eh), (0, 255, 0), 2)

    return img
```

Trong đó : 
- haarcascade_frontalface_default.xml, haarcascade_eye.xml : File download từ OpenCV
- Input : img dạng matrix
- Output : img đã vẽ bounding deteted mặt và mắt

```python
import cv2
from base_camera import BaseCamera
from vision_api.face_detector import face_detect

class Camera(BaseCamera):
    video_source = 0

    @staticmethod
    def set_video_source(source):
        Camera.video_source = source

    @staticmethod
    def frames():
        camera = cv2.VideoCapture(Camera.video_source)
        if not camera.isOpened():
            raise RuntimeError('Could not start camera.')

        while True:
            # read current frame
            _, img = camera.read()

            # encode as a jpeg image and return it
            img = face_detect(img, False)
            yield cv2.imencode('.jpg', img)[1].tobytes()
```

* Run detect_face function và return image đã được detect.

Kết quả : 

![alt text](./img/img3.png)

### 5. Limitations of Streaming
**Vấn đề :**
Không thể truy cập cùng lúc tới Web Server. Mỗi thời điểm chỉ được 1 request.

**Nguyên Nhân  :** 
- Web Server nhận request từ Client, gửi tới function tương ứng
- Function handle trả về kết quả cho Client
- Disconected với Client và thực hiện tiếp các request tiếp theo.

Tuy nhiên, việc streaming videos khiến Web Server không thể xử lý request của Client khác cho tới khi Client hiện tại ngắt kết nối.

**Giải pháp :** 
- Coroutine based web. Flash (gevent)

## II. Stream Videos Over Network

### 1. Overview structure

Để truyền videos giữa các Server thông qua Networks, cần sử dụng Messaging Broker làm trung gian.

![alt text](./img/img6.png)

Một số Message broker phổ biến hiện tại. Với từng loại mesage broker khác nhau, sẽ có ưu và nhược điểm khác nhau.

![alt text](./img/img5.png)

### 2. Use ZeroMQ streaming videos data



-------
Resource : 
- https://blog.miguelgrinberg.com/post/video-streaming-with-flask
- https://docs.opencv.org/3.4.1/d7/d8b/tutorial_py_face_detection.html
- https://tulapdatcamera.wordpress.com/2013/09/04/so-sanh-chuan-nen-h-264-va-mjpeg-trong-camera-ip/
- https://flussonic.com/post/html5-streaming
- https://www.pyimagesearch.com/2019/04/15/live-video-streaming-over-network-with-opencv-and-imagezmq/
- https://stackshare.io/stackups/kafka-vs-rabbitmq-vs-zeromq


Kafka : 
- Mô hình gửi từ Producer bên ngoài :  https://rmoff.net/2018/08/02/kafka-listeners-explained/ 